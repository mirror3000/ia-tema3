module HumanAgent where

import RolitEngine (GameState(..), showBoard, nextStates)

-- Read coordinates from stdin. If the player enters invalid coordinates, the function will let him/her try again.
humanAgent :: GameState -> IO (Int, Int)
humanAgent state = do let brd = board state
                      putStrLn $ showBoard brd ++ "\nYou are " ++ (show $ head $ playerOrder state)
                      coord <- humanInput
                      if (elem coord $ map fst $ nextStates state) then return coord else humanAgent state
                   where humanInput = getLine >>= (\str -> case map read $ words str :: [Int] of [a, b] -> return (a - 1, b - 1)
                                                                                                 _ -> return (-1, -1))

