build: rolit.hs rolitEngine.hs mcts.hs human.hs
	ghc --make -O3 *.hs

run: build
	ghci rolit rolitEngine mcts human

clean:
	rm *.o *.hi
