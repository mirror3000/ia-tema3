module MCTSAgent (mctsAgent) where

import RolitEngine (Player(..), GameState(..), isGameOver, nextStates, winner)
import System.Random (StdGen, randomR, split, newStdGen)
import Data.Maybe (fromJust)
import Data.List (maximumBy, find)

data MCTSTree a b = MCTSNode {content :: a, children :: [(b, MCTSTree a b)]}

data NodeInfo = NodeInfo {played :: Int, score :: Int, state :: GameState}

-- MCTS parameters
iterations = 40
gamesPerIteration = 50
bias = 2

-- MCTS Agent
mctsAgent :: GameState -> IO (Int, Int)
mctsAgent st = do root <- replay iterations $ MCTSNode (NodeInfo 0 0 st) []
                  return $ fst $ maximumBy (\n1 n2 -> compare (nodeScore $ snd n1) $ nodeScore $ snd n2) $ children root
               where replay 0 tree = return tree
                     replay x tree = do gen <- newStdGen
                                        replay (x - 1) $ expandTree tree gen $ head $ playerOrder st

-- Add a new node to the tree
expandTree :: MCTSTree NodeInfo (Int, Int) -> StdGen -> Player -> MCTSTree NodeInfo (Int, Int)
expandTree tree generator player
    | (length expanded) < (length possible) = let (move, st) = possible !! (length expanded)
                                                  newNode = MCTSNode (NodeInfo gamesPerIteration
                                                                               (play generator gamesPerIteration 0 st)
                                                                               st) [] in
                                              tree {children = (move, newNode) : expanded}
    | isGameOver $ board $ state $ content tree = tree
    | otherwise = let (x, n) = maximumBy (\n1 n2 -> compare (ucb (snd n1) tree) $ ucb (snd n2) tree) expanded in
                  tree {children = (x, expandTree n generator player) : [c | c <- expanded, fst c /= x]}
    where play gen count sum st = if count == 0 then sum else let (s2, g2) = randomSimulation st player gen in
                                  play g2 (count - 1) (sum + s2) st
          expandable = (length expanded) < (length possible)
          possible = nextStates $ state $ content tree
          expanded = children tree


-- Simulate a game starting from the given state
randomSimulation :: GameState -> Player -> StdGen -> (Int, StdGen)
randomSimulation st player generator
    | (fst result) == player = (1, snd result)
    | otherwise = (-1, snd result)
    where result = simulate st generator
          simulate s gen
              | isGameOver $ board s = (winner $ board s, gen)
              | otherwise = let (idx, g) = randomR (0, (length $ nextStates s) - 1) gen in
                            simulate (snd $ (nextStates s) !! idx) g

-- Upper confidence bound
ucb :: MCTSTree NodeInfo (Int, Int) -> MCTSTree NodeInfo (Int, Int) -> Float
ucb node parent = f_scr / f_pld + bias * (sqrt $ (log f_t) / f_pld)
                  where f_pld = fromIntegral $ nodePlays node
                        f_scr = fromIntegral $ nodeScore node
                        f_t = fromIntegral $ nodePlays parent
                        nodePlays n = (played $ content n) + (sum $ map (nodePlays . snd) $ children n)

-- Compute the score of the node
nodeScore ::  MCTSTree NodeInfo (Int, Int) -> Int
nodeScore n = (score $ content n) + (sum $ map (nodeScore . snd) $ children n)

