module Rolit (rolit, Player(..)) where

import RolitEngine (Player(..), playGame)
import HumanAgent (humanAgent)
import MCTSAgent (mctsAgent)

rolit :: [Player] -> IO ()
rolit humans = playGame $ concat $ repeat [if elem p humans then humanAgent else mctsAgent | p <- [R, B, Y, G]]
