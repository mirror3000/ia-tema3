module RolitEngine (Player(..), GameState(..), showBoard, isGameOver, nextStates, winner, playGame) where

import Data.Maybe (isJust, isNothing, fromJust)
import Data.Sequence (update, fromList)
import Data.Foldable (toList)
import Control.Monad (when)

data Player = R | B | Y | G deriving (Show, Eq)

type Board = [[Maybe Player]]

data GameState = GameState { playerOrder :: [Player], board :: Board }

-- The starting state of the game
initialState :: GameState
initialState = GameState
               (concat $ repeat [R, B, Y, G]) $
               foldl (\brd (coord, player) -> updateAt coord player brd)
                     (replicate 8 $ replicate 8 Nothing)
                     [((3, 3), Just R), ((3, 4), Just Y), ((4, 3), Just B), ((4, 4), Just G)]

-- Updates the element at (r, c)
updateAt :: (Int, Int) -> Maybe Player -> Board -> Board
updateAt (r, c) x m = toList $ update r (toList $ update c x $ fromList $ m !! r) $ fromList m

-- Test is coordinates are in bounds
inBounds :: (Int, Int) -> Bool
inBounds (row, col) = row >= 0 && row < 8 && col >= 0 && col < 8

-- Make a move on the board
makeMove :: (Int, Int) -> GameState -> GameState
makeMove (row, col) (GameState (p : rest) b) =
    GameState rest $ foldl (\brd coord -> updateAt coord (Just p) brd) b $ (row, col) : (captured (row, col) p b)

-- List of balls captured by placing a ball
captured :: (Int, Int) -> Player -> Board -> [(Int, Int)]
captured (row, col) p b = concat $ map (myTail . dropWhile (\(r, c) -> b !! r !! c /= Just p) . reverse) $
                                       [takeWhile (\(r, c) -> isJust $ b !! r !! c) [pos | k <- [1 .. 8], let pos = (row + k * i, col + k * j), inBounds pos]
                                        | (i, j) <- directions]
                          where myTail [] = []
                                myTail (_ : rest) = rest

-- A list of all the directions in which to search for neighbours
directions :: [(Int, Int)]
directions = [(-1, -1), (-1, 0), (-1, 1), (0, -1), (0, 1), (1, -1), (1, 0), (1, 1)]

-- Converts the board into a user friendly string format
showBoard :: Board -> String
showBoard b = "  1 2 3 4 5 6 7 8\n" ++
              concatMap (\(r, n) -> show n ++ " " ++ showRow r ++ show n ++ "\n") (zip b [1 .. 8]) ++
              "  1 2 3 4 5 6 7 8"
              where showRow = concatMap (\x -> (if isNothing x then "." else show (fromJust x)) ++ " ")

-- Test if game is over
isGameOver :: Board -> Bool
isGameOver = and . concatMap (map isJust)

-- The list of possible next states
nextStates :: GameState -> [((Int, Int), GameState)]
nextStates s = map (\x -> (x, makeMove x s)) $ validMoves (head $ playerOrder s) $ board s

-- List of possible moves
validMoves :: Player -> Board -> [(Int, Int)]
validMoves p b
    | null capturePositions = freeSpots
    | otherwise = capturePositions
    where capturePositions = filter (\pos -> not $ null $ (captured pos p b)) freeSpots
          freeSpots = [(i, j) | i <- [0 .. 7], j <- [0 .. 7], isNothing $ b !! i !! j, hasNeighbours (i, j)]
          hasNeighbours (row, col) = not $ null $ filter isJust $
                                     [b !! r !! c | (i, j) <- directions, let (r, c) = (row + i, col + j), inBounds (r, c)]

-- The winner of the game. If the game is not over it will throw an error.
winner :: Board -> Player
winner brd = fst $ foldl (\(p1, scr1) p2 -> let scr2 = length $ filter (== p2) $ map fromJust $ concat brd in
                                            if scr1 < scr2 then (p2, scr2) else (p1, scr1))
                         (R, -1) [R, B, Y, G]

-- Plays the game using the agents given as argument
playGame :: [GameState -> IO (Int, Int)] -> IO ()
playGame players = play initialState players
                   where play state players
                             | isGameOver $ board state = putStrLn $ (showBoard $ board state) ++ "\nThe winner is " ++ (show $ winner $ board state)
                             | otherwise = (when ((head $ playerOrder state) == R) $ putStrLn $ showBoard $ board state) >>
                                           head players state >>= (\coords -> (when (not $ elem coords $ map fst $ nextStates state) $ error "Invalid move") >>
                                                                               play (makeMove coords state) $ tail players)

